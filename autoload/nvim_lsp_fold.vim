let g:lsp_fold_expr_count = 0

function! nvim_lsp_fold#lsp_fold_expr()
  let g:lsp_fold_expr_count += 1
  return luaeval(printf("require('nvim-lsp-fold').fold_expr(%d)", v:lnum))
endfunction
