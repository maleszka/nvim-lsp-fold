--------------- Scopes
local M = {}
M.util = {}

--------------- Options
M.options = {}

-- This is an alternative to vim.fn.get.
local function get(scope, var, default)
  if scope[var] == nil then
    return default
  end
  return scope[var]
end

-- If enabled, nvim-lsp-fold is enabled even in diff mode. Useful when
-- user wants diff as foldmethod.
M.options.in_diff = get(vim.g, 'nvim_lsp_fold#in_diff', true)

-- Verbosity of nvim-lsp-fold commands:
  -- 0: quiet
  -- 1: errors
  -- 2: warnings
  -- 3: info/debug
M.options.verbosity = get(vim.g, 'nvim_lsp_fold#verbosity', 2)

-- If enabled, update_folds will be called on BufWritePost of registered
-- buffer.
M.options.update_on_save = get(vim.g, 'nvim_lsp_fold#update_on_save', true)

--------------- Structures
-- Will store all currently working clients.
M.fold_data = {}

--------------- Functions
function M.options.reload()
  M.options.in_diff = get(vim.g, 'nvim_lsp_fold#in_diff', true)
  M.options.verbosity = get(vim.g, 'nvim_lsp_fold#verbosity', 2)
  M.options.update_on_save = get(vim.g, 'nvim_lsp_fold#update_on_save', true)
end

function M.util.get(table, pos)
  if table[pos] == nil then
    table[pos] = 0
  end
  return table[pos]
end

-- Here you can configure line offset for you LS!
M.util.offset_table = {}
M.util.offset_table['ccls'] = {1, 2}
M.util.offset_table['cssls'] = {1, 3}
M.util.offset_table['html'] = {1, 3}
M.util.offset_table['jsonls'] = {1, 3}
M.util.offset_table['texlab'] = {1, 2}
M.util.offset_table['vimls'] = {1, 2}


function M.util.start_offset(pos, client_name)
  if M.util.offset_table[client_name] == nil then
    return pos + 1
  end
  return pos + M.util.offset_table[client_name][1]
end

function M.util.end_offset(pos, client_name)
  if M.util.offset_table[client_name] == nil then
    return pos + 2
  end
  return pos + M.util.offset_table[client_name][2]
end

function M.util.set_buf_option(bufnr, name, value, rep)
  local wins = vim.fn.win_findbuf(bufnr)

  -- If buffer is not associated to any window, discard.
  if vim.tbl_isempty(wins) then
    return
  end

  for _, win in ipairs(wins) do
    if vim.api.nvim_win_get_option(win, name) ~= value or rep == true then
      vim.api.nvim_win_set_option(win, name, value)
    end
  end
end

-- Severity: 1 - info, 2 - warning, 3 - error
function M.util.notify(msg, severity)
  if severity == nil then severity = 2
  elseif severity > 3 then severity = 3
  elseif severity < 1 then severity = 1
  end

  if severity > 3 - M.options.verbosity then
    local msg_prefix = {[1] = 'info: ', [2] = 'Warning: ', [3] = 'Error: '}
    vim.notify(msg_prefix[severity] .. msg, severity)
  end
end

-- Checks if client supports textDocument/foldingRange.
function M.is_valid(client)
  return client.server_capabilities.foldingRangeProvider == true
end

-- Registers a new client and associates a buffer with it.
function M.register_buffer(client, bufnr)
  -- Discard previous client and clean buffer.
  M.fold_data[bufnr] = {}
  M.fold_data[bufnr].client_id = client.id
  M.fold_data[bufnr].data = {}

  -- Debug info
  M.util.notify('registered a buffer (' .. bufnr .. ') for ' .. client.name .. ' lsp client', 1)
end

-- Delete fold data of a deleted or wiped buffer.
function M.delete_buffer(bufnr)
  M.fold_data[bufnr] = nil

  -- Debug info
  M.util.notify('purged fold data of the buffer ' .. bufnr, 1)
end

function M.buffer_setup(client, bufnr)
  -- Auto-delete wiped buffers.
  -- It is important, because it makes it possible to reuse a bufnr of the
  -- deleted buffer.
  vim.api.nvim_command("augroup NvimLspFoldWipe")
  vim.api.nvim_command("autocmd! * <buffer>")
  vim.api.nvim_command("autocmd BufWipeout <buffer> execute \"lua require'nvim-lsp-fold'.delete_buffer(\" . expand('<abuf>') . \")\"")
  vim.api.nvim_command("augroup END")

  -- Automatically update folds after save.
  if M.options.update_on_save == true then
    vim.api.nvim_command("augroup NvimLspFoldUpdate")
    vim.api.nvim_command("autocmd! * <buffer>")
    vim.api.nvim_command("autocmd BufWritePost <buffer> execute \"lua require'nvim-lsp-fold'.update_folds(\" . expand('<abuf>') . \")\"")
    vim.api.nvim_command("augroup END")
  end
end

-- Registers a new client and updates foldings.
function M.on_attach(client, bufnr)
  -- Reload options
  M.options.reload()

  if M.is_valid(client) and bufnr ~= -1 then
    M.register_buffer(client, bufnr)
    M.buffer_setup(client, bufnr)
  else
    M.util.notify(client.name .. " language server does not support textDocument/foldingRange!", 3)
  end
end

function M.update_folds(bufnr)
  -- If bufnr is nil, use current.
  if bufnr == nil then
    bufnr = vim.api.nvim_get_current_buf()

    -- Info
    M.util.notify('bufnr not specified, using the current one (' .. bufnr .. ')', 1)
  end

  -- Don't use foldexpr when Neovim is in diff mode.
  local diff_mode = vim.api.nvim_win_get_option(vim.api.nvim_get_current_win(), 'diff')
  if M.options.in_diff == false and diff_mode == true then
    M.util.set_buf_option(bufnr, 'foldmethod', 'diff', false)
    return
  end

  -- When user calls update_folds manually not preceding it with
  -- on_attach, bufnr can be not associated to any client.
  if M.fold_data[bufnr] == nil then
    M.util.notify('there is no client associated with the buffer ' .. bufnr .. '; call on_attach(client, bufnr) first!', 2)
    return
  end

  -- Request LSP client.
  local client = vim.lsp.get_client_by_id(M.fold_data[bufnr].client_id)
  local params = { uri = vim.uri_from_bufnr(bufnr) }
  if vim.version().api_level < 8 then
    client.request('textDocument/foldingRange', {textDocument = params}, M.fold_range_handler, bufnr)
  else
    client.request('textDocument/foldingRange', {textDocument = params}, M.fold_range_handler_new, bufnr)
  end
  M.util.notify('requesting foldingRange from client (id ' .. client.id .. ') for buffer ' .. bufnr, 1)
end

-- Actually, the only breaking change to the functionality of this handler
-- are function arguments. This is a small compatibility layer needed
-- for api_level >= 8.
function M.fold_range_handler_new(_, result, ctx, _)
  -- Create a dummy error message
  local err = {
    message = "Unknown error",
    code = 1
  }

  -- Execute the old one
  M.fold_range_handler(err, nil, result, ctx.client_id, ctx.bufnr)
end

-- Params: err, method, result, client_id, bufnr
function M.fold_range_handler(err, _, result, client_id, bufnr)
  if vim.api.nvim_buf_is_valid(bufnr) == false or M.fold_data[bufnr] == nil then
    -- Because the handler is asynchronous, it is possible that the checked
    -- buffer has been deleted and now is not valid.
    M.util.notify('buffer ' .. bufnr .. ' has been wiped since last request', 2)
    return
  end

  -- Get info about client
  local client = vim.lsp.get_client_by_id(client_id)

  -- Error
  if result == nil then
    if M.options.quiet == false then
      vim.notify('Cannot create folds: [' .. client.name .. '] ' .. err.message .. ' (' .. err.code .. ')')
      M.util.notify(client.name .. ' server error: ' .. err.message .. ' (code: ' .. err.code .. ')', 3)
    end
    return
  end

  -- Reset data
  M.fold_data[bufnr].data = {}

  M.util.notify('processing folds (' .. vim.tbl_count(result) .. ')', 1)

  -- Copy result to data.
  for _, fold in ipairs(result) do
    fold.startLine = M.util.start_offset(fold.startLine, client.name)
    fold.endLine = M.util.end_offset(fold.endLine, client.name)

    if M.fold_data[bufnr].data[fold.startLine] == nil then M.fold_data[bufnr].data[fold.startLine] = 0 end
    if M.fold_data[bufnr].data[fold.endLine] == nil then M.fold_data[bufnr].data[fold.endLine] = 0 end

    M.fold_data[bufnr].data[fold.startLine] = M.fold_data[bufnr].data[fold.startLine] + 1
    M.fold_data[bufnr].data[fold.endLine] = M.fold_data[bufnr].data[fold.endLine] - 1
  end

  -- Fill empty spaces in data.
  local lines = vim.api.nvim_buf_line_count(bufnr)
  for line = 1,lines,1 do
    M.fold_data[bufnr].data[line] = M.util.get(M.fold_data[bufnr].data, line - 1) + M.util.get(M.fold_data[bufnr].data, line)
  end

  -- Inspect folds.
  M.util.notify('fold_level array: ' .. vim.inspect(M.fold_data[bufnr].data), 1)

  -- Set foldmethod and foldexpr
  M.util.set_buf_option(bufnr, 'foldexpr', 'nvim_lsp_fold#lsp_fold_expr()', false)
  M.util.set_buf_option(bufnr, 'foldmethod', 'expr', true)
end

function M.fold_expr(lnum)
  local win = vim.api.nvim_get_current_win()
  local bufnr = vim.api.nvim_win_get_buf(win)

  -- Foldmethod is local to window, so it possible that buffer in window
  -- has been changed.
  if M.fold_data[bufnr] == nil then
    vim.api.nvim_win_set_option(win, 'foldmethod', 'manual')
    vim.api.nvim_win_set_option(win, 'foldexpr', '0')
    M.util.notify('buffer ' .. bufnr .. ' no longer belongs to the window (id ' .. win .. ')', 1)
    return -1
  end

  -- If buffer is not indexed yet.
  if M.fold_data[bufnr].data == {} then
    return 0
  end

  return M.fold_data[bufnr].data[lnum]
end

return {
  on_attach = M.on_attach,
  delete_buffer = M.delete_buffer,
  update_folds = M.update_folds,
  fold_expr = M.fold_expr,
  options = M.options,
}
